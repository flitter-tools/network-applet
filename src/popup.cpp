/*
    Network-applet - tray helper for enable disable network\tor\vpn.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "popup.h"
#include "ui_popup.h"

#include "network.h"
#include "settingsdialog.h"
#include "lwcustomitem.h"

#include <QDesktopWidget>
#include <QSettings>
#include <QDir>
#include <QProcess>
#include <QMessageBox>

popup::popup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::popup)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);    

    network = new Network;

    updateFile();

    ui->l_network->setPixmap(QIcon::fromTheme("preferences-system-network").pixmap(16, 16));

    for (auto item : network->listInterfaces())
    {
        if (item == "lo") // ignore loop since it's useless to disable or enable and always sayd it's down
            continue;

        LWCustomItem *citem = new LWCustomItem(this, item);
        QListWidgetItem *lwitem = new QListWidgetItem(ui->lw_networks);
        lwitem->setSizeHint(citem->sizeHint());
        ui->lw_networks->setItemWidget(lwitem, citem);
    }

    this->adjustSize(); // resize widget depends on contents

    this->move(QApplication::desktop()->width() - this->width(),\
                   QApplication::desktop()->height() - this->height() - 27);
}

popup::~popup()
{
    delete network;
    delete ui;
}

void popup::updateFile()
{
    for (int i = 0; i < ui->lw_networks->count(); ++i)
    {
        QListWidgetItem *item = ui->lw_networks->item(i);
        LWCustomItem *citem = static_cast<LWCustomItem*>(ui->lw_networks->itemWidget(item));
        citem->updateBytes();
    }

    QSettings settings(QDir::homePath() + "/.config/flitter/network.conf", QSettings::IniFormat);
    m_ovpnfile = settings.value("ovpnfile").toString();

    if (m_ovpnfile.isEmpty() || m_ovpnfile.isNull())
        ui->btn_openvpn->setEnabled(false);
    else
        ui->btn_openvpn->setEnabled(true);

    if (network->checkProcess("/usr/bin/tor"))
    {
        ui->btn_tor->setIcon(QIcon::fromTheme("network-disconnect"));
        ui->btn_tor->setText("Stop Tor");
    }
    else
    {
        ui->btn_tor->setIcon(QIcon::fromTheme("gtk-network"));
        ui->btn_tor->setText("Start Tor");
    }

    if (network->checkProcess("openvpn"))
    {
        ui->btn_openvpn->setEnabled(false);
        ui->btn_openvpn->setText("OpenVPN is started");
    }
    else
    {
        ui->btn_openvpn->setEnabled(true);
        ui->btn_openvpn->setText("Start OpenVPN");
    }
}

void popup::on_btn_openvpn_clicked()
{
    this->hide();
    network->start_vpn(m_ovpnfile);
}

void popup::on_btn_tor_clicked()
{
    this->hide();
    network->start_tor();
}

void popup::on_btn_settings_clicked()
{
    SettingsDialog *settings = new SettingsDialog;
    settings->show();
}
