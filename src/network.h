/*
    Network-applet - tray helper for enable disable network\tor\vpn.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QString>

class Network : public QObject
{
    Q_OBJECT

    public:
        explicit Network(QObject *parent = nullptr);
        ~Network();

        bool checkProcess(const QString &name);
        bool checkIfup(const QString &interface);
        bool checkIfAvailable(const QString &interface);
        void start_tor();
        void start_vpn(const QString &ovpnfile);
        void ifupdown(const QString &interface);
        QStringList listInterfaces();
        QString rxbytes(const QString &interface);
        QString rxerrors(const QString &interface);
        QString rxdropped(const QString &interface);
};

#endif // NETWORK_H
