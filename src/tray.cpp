/*
    Network-applet - tray helper for enable disable network\tor\vpn
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "tray.h"
#include "network.h"

#include <QIcon>
#include <QCoreApplication>
#include <QImage>
#include <QColor>
#include <QPainter>

tray::tray(QSystemTrayIcon *parent) : QSystemTrayIcon(parent)
{
    a_quit = new QAction("Quit", this);
    connect(a_quit, &QAction::triggered, qApp, &QCoreApplication::quit);

    tmenu = new QMenu;
    tmenu->addAction(a_quit);

    ipopup = new popup;

    network = new Network;

    setContextMenu(tmenu);
    connect(this, &QSystemTrayIcon::activated, this, &tray::tpopup);

    updateIcon();
}

tray::~tray()
{
    delete network;
    delete ipopup;
    delete a_quit;
    delete tmenu;
}

void tray::tpopup(QSystemTrayIcon::ActivationReason reason)
{
    if (reason != 1)
    {
        ipopup->show();
        ipopup->updateFile();
    }

    updateIcon();
}

void tray::updateIcon()
{
    QIcon icon = QIcon::fromTheme("desktop-connected");
    QPixmap pix = icon.pixmap(icon.availableSizes().first());

    QPainter painter(&pix);
    painter.setCompositionMode(QPainter::CompositionMode_SourceIn);

    QColor color;

    if (!network->checkIfup("eth0"))
        color = "#FF2F2F";
    //else if (!Network::checkIfAvailable("eth0"))
    //    color = "#DEC623";
    else
        color = "#CDD6DA";

    painter.fillRect(pix.rect(), color);

    if (network->checkProcess("openvpn") || network->checkProcess("/usr/bin/tor"))
    {
        // don't know what will be "best" to draw it by Qt or use svg (maybe convert to png?)
        // anyway it's work better then my prev code (at least in terms of memory usage)

        QImage image(":/images/folder-remote-vpn.svg");
        painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
        painter.drawImage(0, 0, image);
    }

    painter.end();
    pix = pix.scaled(14, 14, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    this->setIcon(QIcon(pix));
}
