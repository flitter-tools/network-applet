#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QSettings>
#include <QFileDialog>
#include <QProcess>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <QMessageBox>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    QFile file("/etc/resolv.conf");
    file.open(QIODevice::ReadOnly);
    ui->le_address->setText(file.readAll().split(' ')[1]);
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::on_btn_default_accepted()
{
    QSettings settings(QDir::homePath() + "/.config/flitter/network.conf", QSettings::IniFormat);
    settings.setValue("ovpnfile", ui->le_file->text());

    QFile file(QDir::homePath() + "/.cache/resolv.sh");
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream ts(&file);
        ts << "echo nameserver " + ui->le_address->text() + " > /etc/resolv.conf";

        if (ts.status() == ts.WriteFailed)
        {
            QMessageBox::warning(nullptr, "Error", "Can't write file.", QMessageBox::Ok);
        }
        else
        {
            file.setPermissions(QFileDevice::ExeOther | QFileDevice::ExeUser | QFileDevice::ExeGroup | QFileDevice::ExeOwner | QFileDevice::ReadUser | QFileDevice::ReadGroup | QFileDevice::ReadOwner | QFileDevice::ReadOther);

            QProcess proc;
            proc.start("qsudo " + QDir::homePath() + "/.cache/resolv.sh");
            proc.waitForFinished();

            file.remove();
            file.close();
        }
    }
    else
    {
        QMessageBox::warning(nullptr, "Error", "Can't write file.", QMessageBox::Ok);
    }

    this->hide();
}

void SettingsDialog::on_btn_search_clicked()
{
    QString file = QFileDialog::getOpenFileName(this,
          tr("Open OpenVPN file"), QDir::homePath(), tr("OpenVPN Files (*.ovpn)"));

    ui->le_file->setText(file);
}

void SettingsDialog::on_btn_default_rejected()
{
    this->hide();
}
