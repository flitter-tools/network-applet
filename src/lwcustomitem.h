#ifndef LWCUSTOMITEM_H
#define LWCUSTOMITEM_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QVBoxLayout>
#include <QObject>

#include "network.h"

class LWCustomItem : public QWidget
{
    Q_OBJECT

    public:
        explicit LWCustomItem(QWidget *parent = nullptr, const QString &name = "eth0");
        virtual ~LWCustomItem();

        void updateBytes();

    private:
        QLabel *label;
        QLabel *bytes;
        QPushButton *connect;
        QVBoxLayout *layout;
        Network *network;

        QString int_name;
};

#endif // LWCUSTOMITEM_H
