/*
    Network-applet - tray helper for enable disable network\tor\vpn.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef POPUP_H
#define POPUP_H

#include <QWidget>
#include "network.h"

namespace Ui {class popup;}

class popup : public QWidget
{
    Q_OBJECT

    public:
        explicit popup(QWidget *parent = nullptr);
        ~popup();

        void updateFile();

    private slots:
        void on_btn_openvpn_clicked();
        void on_btn_tor_clicked();
        void on_btn_settings_clicked();

    private:
        Ui::popup *ui;
        QString m_ovpnfile;
        Network *network;
};

#endif // POPUP_H
