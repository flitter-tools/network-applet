#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QWidget>

namespace Ui {class SettingsDialog;}

class SettingsDialog : public QWidget
{
    Q_OBJECT

    public:
        explicit SettingsDialog(QWidget *parent = 0);
        ~SettingsDialog();

    private slots:
        void on_btn_default_accepted();
        void on_btn_search_clicked();

        void on_btn_default_rejected();

private:
        Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
