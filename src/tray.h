/*
    Network-applet - tray helper for enable disable network\tor\vpn.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef TRAY_H
#define TRAY_H

#include <QWidget>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>
#include <QPixmap>

#include "popup.h"
#include "network.h"

class tray : public QSystemTrayIcon
{
    Q_OBJECT

    public:
        explicit tray(QSystemTrayIcon *parent = 0);
        virtual ~tray();

    private slots:
        void tpopup(QSystemTrayIcon::ActivationReason reason);
        void updateIcon();

    private:
        QAction *a_quit;
        QMenu *tmenu;
        popup *ipopup;
        Network *network;
};

#endif // TRAY_H
