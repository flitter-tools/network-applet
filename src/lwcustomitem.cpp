#include "lwcustomitem.h"

#include <QMessageBox>

LWCustomItem::LWCustomItem(QWidget *parent, const QString &name) : QWidget (parent)
{
    label = new QLabel;
    bytes = new QLabel;
    connect = new QPushButton;
    layout = new QVBoxLayout;

    layout->addWidget(label);
    layout->addWidget(bytes);
    layout->addWidget(connect);

    label->setText(name);

    network = new Network;
    int_name = name;

    bytes->setStyleSheet("font-size: 10px; color: #757575;");

    updateBytes();

    QObject::connect(connect, &QPushButton::clicked, [&](){network->ifupdown(int_name);});

    this->setLayout(layout);
}

LWCustomItem::~LWCustomItem()
{
    delete network;
    delete label;
    delete connect;
    delete layout;
}

void LWCustomItem::updateBytes()
{
    if (network->checkIfup(int_name))
        connect->setText("Disconnect");
    else
        connect->setText("Connect");

    bytes->setText("Bytes: " + network->rxbytes(int_name) + " | Dropped: " + network->rxdropped(int_name) + " | Error: " + network->rxerrors(int_name));
}
