/*
    Network-applet - tray helper for enable disable network\tor\vpn.
    Copyright (C) 2018 Volk_Milit (aka Ja'Virr-Dar)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "network.h"

#include <QDirIterator>
#include <QFile>
#include <QTextStream>
#include <QEventLoop>
#include <QProcess>
#include <QCoreApplication>
#include <QTime>

Network::Network(QObject *parent) : QObject (parent){}
Network::~Network(){}

bool Network::checkProcess(const QString &name)
{
    bool found = false;
    QDirIterator it("/proc");

    QString line;

    while (it.hasNext())
    {
        QFile file(it.next() + "/cmdline");

        if (!file.exists())
            continue;

        file.open(QIODevice::ReadOnly);
        QTextStream in(&file);

        line = in.readLine();

        if (line.contains(name))
        {
            file.close();
            found = true;
            break;
        }

        file.close();
    }

    return found;
}

QStringList Network::listInterfaces()
{
    QDir sysnet("/sys/class/net/");
    QStringList interfaces = sysnet.entryList(QDir::NoDotAndDotDot | QDir::AllDirs);

    return interfaces;
}

bool Network::checkIfup(const QString &interface)
{
    QFile it("/sys/class/net/" + interface + "/operstate");
    if (!it.open(QIODevice::ReadOnly))
        return false;

    QString status = it.readAll();

    if (status == "up\n")
        return true;
    else
        return false;
}

QString Network::rxbytes(const QString &interface)
{
    QFile it("/sys/class/net/" + interface + "/statistics/rx_bytes");
    if (!it.open(QIODevice::ReadOnly))
        return "";

    QString ret = it.readAll();

    return ret.remove("\n");
}

QString Network::rxdropped(const QString &interface)
{
    QFile it("/sys/class/net/" + interface + "/statistics/rx_dropped");
    if (!it.open(QIODevice::ReadOnly))
        return "";

    QString ret = it.readAll();

    return ret.remove("\n");
}

QString Network::rxerrors(const QString &interface)
{
    QFile it("/sys/class/net/" + interface + "/statistics/rx_errors");
    if (!it.open(QIODevice::ReadOnly))
        return "";

    QString ret = it.readAll();

    return ret.remove("\n");
}

bool Network::checkIfAvailable(const QString &interface)
{
    QFile it("/sys/class/net/" + interface + "/statistics/rx_bytes");
    if (!it.open(QIODevice::ReadOnly))
        return false;

    QString status;
    QString oldstatus;

    bool avail = false;

    for (int i = 0; i < 3; i++)
    {
        status = it.readAll();

        if (!status.contains(oldstatus))
            avail = true;

        oldstatus = status;

        it.seek(0);

        QTime dieTime = QTime::currentTime().addSecs(2);
        while (QTime::currentTime() < dieTime)
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

    it.close();

    return avail;
}

void Network::ifupdown(const QString &interface)
{
    bool avail = this->checkIfup(interface);

    QProcess *proc = new QProcess;

    if (avail)
        proc->startDetached("qsudo ifdown " + interface);
    else
        proc->startDetached("qsudo ifup " + interface);

    proc->waitForFinished();

    delete proc;
}

void Network::start_vpn(const QString &ovpnfile)
{
    if (checkProcess("openvpn"))
        return;

    QProcess *proc = new QProcess;
    proc->startDetached("x-terminal-emulator -T openvpn -e sudo openvpn " + ovpnfile);

    proc->waitForFinished();

    delete proc;
}

void Network::start_tor()
{
    QString startstop = "start";
    if (checkProcess("/usr/bin/tor"))
        startstop = "stop";

    QProcess *proc = new QProcess;
    proc->start("qsudo service tor " + startstop);

    proc->waitForFinished();

    delete proc;
}
